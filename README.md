# Repozytorium dla narzędzi do statycznej analizy kodu w C.

Do analizy wykorzystywane jest open-source'owe narzędzie CppCheck.

Strona domowa:
http://www.cppcheck.net


##Wymagania:

*	Zainstalowany CppCheck
*	Zainstalowany Git
*	CppCheck dodany do zmiennej środowiskowej PATH której używa Git

##Instrukcja

1.	###Instalacja

    Ze strony <http://www.cppcheck.net/> ściągamy instalator dla Windows 64-bit
	
	
2.	###Konfiguracja zmiennej środowiskowej (na przykładzie Msys2, jeżeli używasz SourceTree to prawdopodobnie należy zrobić to w GitBash'u)

    W ulubionym edytorze tekstu Vim otwieramy plik ~/.bashrc i na jego końcu dodajemy:
	
	```export PATH=$PATH:/c/Program\ Files/Cppcheck```
	
    zakładając, że CppCheck został instalowany w domyślnej lokalizacji.
    Aby zaktualizować PATH można zresetować MSYS'a lub wywołać w konsoli:
    
	```. ~/.bashrc```
	
	
3.	###Użytkowanie

    1.	####GUI
	
        Obsługa CppCheck w GUI jest prosta. 	
		Start->Programy->CppCheck a następnie wewnątrz aplikacji Analyze->Directory i wybieramy katalog do analizy.
			
    2.	####Konsola
	
		W repozytorium znajdują się pliki .sh:
           *	check_all.sh
           *	check_warning.sh
           *	check_error.sh
           *	check_style.sh
		Wszystkie jako parametr przyjmują scieżkę do pliku/katalogu który mają sprawdzić np.
           ```./check_style.sh src/``` sprawdzi wszyskie błędy stylistyczne w podkatalogu "src"
           ```./check_error.sh src/main.c``` sprawdzi wszystkie błędy w pliku "src/main.c"
		Drukowane w konsoli komunikaty zawierają na początku informację o pliku i linijce do której się odnoszą.
		Po opis użytej konfiguracji CppCheck odsyłam do 
		
		```cppcheck -h```	
		
		lub instrukcji
		
		<http://cppcheck.sourceforge.net/manual.pdf>
		
		**UWAGA** Rezultaty są wyświetlane w konsoli przez stream stderr więc pipe'owanie komend w stylu
		
		```./check_all.sh src/ | grep "null"```
		
		nie zadziała z racji tego, że pipe czyta ze standardowego wyjścia stdout, należy przekierować 
		stderr do stdout więc powyższa komenda działać będzie w takiej wersji
		
		```./check_all.sh src/ 2>&1 | grep "null"```
                              
	3.	####**Git Hooks**
	
        hooki w gicie to skrypty odpalane po stronie klienta (lokalnie) przed lub po jakiś konkretnych akcjach w repozytorium. 
        W przypadku statycznej analizy możemy dodać hook pre-commit uruchamiający się przy próbie commitowanie. W przypadku 
        gdy statyczna analiza wyłapie jakieś poważne błędy commit zostanie anulowany i dostaniemy informacje zwrotną z opisem błędu.
        Hook dodajemy kopiując go z mojego repozytorium
		
        <https://bitbucket.org/hubert_melchert/git-config/src/master/hooks/>
			
        do repozytorium w którym ma funkcjonować
        Hooki znajdują się zawsze wewnątrz repozytorium lokalnego w ```.git/hooks```
		
	4.	####Vim
	
        Po konfigurację cppcheck z vimem odsyłam do repozytorium z moją konfiguracją vim'a
        https://bitbucket.org/hubert_melchert/vimrc/src/master/
		
		
#TODO:
*	integracja z Atollic True Studio


